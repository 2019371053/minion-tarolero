#!/usr/bin/env node
const minion = require("../src/index");
const yargs = require("yargs");
const { hideBin } = require("yargs/helpers");

const argv = yargs(hideBin(process.argv)).argv;

if (argv.veces) {
    minion.printMinion(argv.veces);
}
