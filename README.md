# Minion Tarolero

This is a library that display random minions the times that you want!

## Installation
use the node package manager [npm](https://www.npmjs.com/) to install this dependency.

```bash
npm install -g minion-tarolero
```

## Usage
```bash
> minion-tarolero --veces=5
```

## License
[MIT](https://choosealicense.com/licenses/mit/)